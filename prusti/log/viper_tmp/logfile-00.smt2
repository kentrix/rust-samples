(get-info :version)
; (:version "4.8.6")
; Started: 2021-01-27 11:28:18
; Silicon.version: 1.1-SNAPSHOT (69b08d23+@(detached))
; Input file: dummy.vpr
; Verifier id: 00
; ------------------------------------------------------------
; Begin preamble
; ////////// Static preamble
; 
; ; /z3config.smt2
(set-option :print-success true) ; Boogie: false
(set-option :global-decls true) ; Boogie: default
(set-option :auto_config false) ; Usually a good idea
(set-option :smt.restart_strategy 0)
(set-option :smt.restart_factor |1.5|)
(set-option :smt.case_split 3)
(set-option :smt.delay_units true)
(set-option :smt.delay_units_threshold 16)
(set-option :nnf.sk_hack true)
(set-option :type_check true)
(set-option :smt.bv.reflect true)
(set-option :smt.mbqi false)
(set-option :smt.qi.eager_threshold 100)
(set-option :smt.qi.cost "(+ weight generation)")
(set-option :smt.qi.max_multi_patterns 1000)
(set-option :smt.phase_selection 0) ; default: 3, Boogie: 0
(set-option :sat.phase caching)
(set-option :sat.random_seed 0)
(set-option :nlsat.randomize true)
(set-option :nlsat.seed 0)
(set-option :nlsat.shuffle_vars false)
(set-option :fp.spacer.order_children 0) ; Not available with Z3 4.5
(set-option :fp.spacer.random_seed 0) ; Not available with Z3 4.5
(set-option :smt.arith.random_initial_value true) ; Boogie: true
(set-option :smt.random_seed 0)
(set-option :sls.random_offset true)
(set-option :sls.random_seed 0)
(set-option :sls.restart_init false)
(set-option :sls.walksat_ucb true)
(set-option :model.v2 true)
; 
; ; /preamble.smt2
(declare-datatypes () ((
    $Snap ($Snap.unit)
    ($Snap.combine ($Snap.first $Snap) ($Snap.second $Snap)))))
(declare-sort $Ref 0)
(declare-const $Ref.null $Ref)
(declare-sort $FPM)
(declare-sort $PPM)
(define-sort $Perm () Real)
(define-const $Perm.Write $Perm 1.0)
(define-const $Perm.No $Perm 0.0)
(define-fun $Perm.isValidVar ((p $Perm)) Bool
	(<= $Perm.No p))
(define-fun $Perm.isReadVar ((p $Perm) (ub $Perm)) Bool
    (and ($Perm.isValidVar p)
         (not (= p $Perm.No))
         (< p $Perm.Write)))
(define-fun $Perm.min ((p1 $Perm) (p2 $Perm)) Real
    (ite (<= p1 p2) p1 p2))
(define-fun $Math.min ((a Int) (b Int)) Int
    (ite (<= a b) a b))
(define-fun $Math.clip ((a Int)) Int
    (ite (< a 0) 0 a))
; ////////// Sorts
(declare-sort $SnapshotMirrors$)
(declare-sort $FVF<$Ref>)
; ////////// Sort wrappers
; Declaring additional sort wrappers
(declare-fun $SortWrappers.IntTo$Snap (Int) $Snap)
(declare-fun $SortWrappers.$SnapToInt ($Snap) Int)
(assert (forall ((x Int)) (!
    (= x ($SortWrappers.$SnapToInt($SortWrappers.IntTo$Snap x)))
    :pattern (($SortWrappers.IntTo$Snap x))
    :qid |$Snap.$SnapToIntTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.IntTo$Snap($SortWrappers.$SnapToInt x)))
    :pattern (($SortWrappers.$SnapToInt x))
    :qid |$Snap.IntTo$SnapToInt|
    )))
(declare-fun $SortWrappers.BoolTo$Snap (Bool) $Snap)
(declare-fun $SortWrappers.$SnapToBool ($Snap) Bool)
(assert (forall ((x Bool)) (!
    (= x ($SortWrappers.$SnapToBool($SortWrappers.BoolTo$Snap x)))
    :pattern (($SortWrappers.BoolTo$Snap x))
    :qid |$Snap.$SnapToBoolTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.BoolTo$Snap($SortWrappers.$SnapToBool x)))
    :pattern (($SortWrappers.$SnapToBool x))
    :qid |$Snap.BoolTo$SnapToBool|
    )))
(declare-fun $SortWrappers.$RefTo$Snap ($Ref) $Snap)
(declare-fun $SortWrappers.$SnapTo$Ref ($Snap) $Ref)
(assert (forall ((x $Ref)) (!
    (= x ($SortWrappers.$SnapTo$Ref($SortWrappers.$RefTo$Snap x)))
    :pattern (($SortWrappers.$RefTo$Snap x))
    :qid |$Snap.$SnapTo$RefTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$RefTo$Snap($SortWrappers.$SnapTo$Ref x)))
    :pattern (($SortWrappers.$SnapTo$Ref x))
    :qid |$Snap.$RefTo$SnapTo$Ref|
    )))
(declare-fun $SortWrappers.$PermTo$Snap ($Perm) $Snap)
(declare-fun $SortWrappers.$SnapTo$Perm ($Snap) $Perm)
(assert (forall ((x $Perm)) (!
    (= x ($SortWrappers.$SnapTo$Perm($SortWrappers.$PermTo$Snap x)))
    :pattern (($SortWrappers.$PermTo$Snap x))
    :qid |$Snap.$SnapTo$PermTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$PermTo$Snap($SortWrappers.$SnapTo$Perm x)))
    :pattern (($SortWrappers.$SnapTo$Perm x))
    :qid |$Snap.$PermTo$SnapTo$Perm|
    )))
; Declaring additional sort wrappers
(declare-fun $SortWrappers.$SnapshotMirrors$To$Snap ($SnapshotMirrors$) $Snap)
(declare-fun $SortWrappers.$SnapTo$SnapshotMirrors$ ($Snap) $SnapshotMirrors$)
(assert (forall ((x $SnapshotMirrors$)) (!
    (= x ($SortWrappers.$SnapTo$SnapshotMirrors$($SortWrappers.$SnapshotMirrors$To$Snap x)))
    :pattern (($SortWrappers.$SnapshotMirrors$To$Snap x))
    :qid |$Snap.$SnapTo$SnapshotMirrors$To$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$SnapshotMirrors$To$Snap($SortWrappers.$SnapTo$SnapshotMirrors$ x)))
    :pattern (($SortWrappers.$SnapTo$SnapshotMirrors$ x))
    :qid |$Snap.$SnapshotMirrors$To$SnapTo$SnapshotMirrors$|
    )))
; Declaring additional sort wrappers
(declare-fun $SortWrappers.$FVF<$Ref>To$Snap ($FVF<$Ref>) $Snap)
(declare-fun $SortWrappers.$SnapTo$FVF<$Ref> ($Snap) $FVF<$Ref>)
(assert (forall ((x $FVF<$Ref>)) (!
    (= x ($SortWrappers.$SnapTo$FVF<$Ref>($SortWrappers.$FVF<$Ref>To$Snap x)))
    :pattern (($SortWrappers.$FVF<$Ref>To$Snap x))
    :qid |$Snap.$SnapTo$FVF<$Ref>To$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$FVF<$Ref>To$Snap($SortWrappers.$SnapTo$FVF<$Ref> x)))
    :pattern (($SortWrappers.$SnapTo$FVF<$Ref> x))
    :qid |$Snap.$FVF<$Ref>To$SnapTo$FVF<$Ref>|
    )))
; ////////// Symbols
(declare-fun mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$<Int> (Int Int) Int)
; Declaring symbols related to program functions (from program analysis)
(declare-fun builtin$unreach_bool__$TY$__$bool$ ($Snap) Bool)
(declare-fun builtin$unreach_bool__$TY$__$bool$%limited ($Snap) Bool)
(declare-const builtin$unreach_bool__$TY$__$bool$%stateless Bool)
(declare-fun read$ ($Snap) $Perm)
(declare-fun read$%limited ($Snap) $Perm)
(declare-const read$%stateless Bool)
(declare-fun m_max__$TY$__$int$$$int$$$int$ ($Snap Int Int) Int)
(declare-fun m_max__$TY$__$int$$$int$$$int$%limited ($Snap Int Int) Int)
(declare-fun m_max__$TY$__$int$$$int$$$int$%stateless (Int Int) Bool)
; Snapshot variable to be used during function verification
(declare-fun s@$ () $Snap)
; Declaring predicate trigger functions
(declare-fun DeadBorrowToken$%trigger ($Snap Int) Bool)
(declare-fun i32%trigger ($Snap $Ref) Bool)
(declare-fun tuple0$%trigger ($Snap $Ref) Bool)
; ////////// Uniqueness assumptions from domains
; ////////// Axioms
; End preamble
; ------------------------------------------------------------
; State saturation: after preamble
(set-option :timeout 100)
(check-sat)
; unknown
; ---------- FUNCTION builtin$unreach_bool__$TY$__$bool$----------
(declare-fun result@0@00 () Bool)
; ----- Well-definedness of specifications -----
(push) ; 1
(assert (= s@$ $Snap.unit))
(assert false)
(pop) ; 1
(assert (forall ((s@$ $Snap)) (!
  (=
    (builtin$unreach_bool__$TY$__$bool$%limited s@$)
    (builtin$unreach_bool__$TY$__$bool$ s@$))
  :pattern ((builtin$unreach_bool__$TY$__$bool$ s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (as builtin$unreach_bool__$TY$__$bool$%stateless  Bool)
  :pattern ((builtin$unreach_bool__$TY$__$bool$%limited s@$))
  )))
; ---------- FUNCTION read$----------
(declare-fun result@1@00 () $Perm)
; ----- Well-definedness of specifications -----
(push) ; 1
(declare-const $t@5@00 $Snap)
(assert (= $t@5@00 ($Snap.combine ($Snap.first $t@5@00) ($Snap.second $t@5@00))))
(assert (= ($Snap.first $t@5@00) $Snap.unit))
; [eval] none < result
(assert (< $Perm.No result@1@00))
(assert (= ($Snap.second $t@5@00) $Snap.unit))
; [eval] result < write
(assert (< result@1@00 $Perm.Write))
(pop) ; 1
(assert (forall ((s@$ $Snap)) (!
  (= (read$%limited s@$) (read$ s@$))
  :pattern ((read$ s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (as read$%stateless  Bool)
  :pattern ((read$%limited s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (let ((result@1@00 (read$%limited s@$))) (and
    (< $Perm.No result@1@00)
    (< result@1@00 $Perm.Write)))
  :pattern ((read$%limited s@$))
  )))
; ---------- FUNCTION m_max__$TY$__$int$$$int$$$int$----------
(declare-fun _1@2@00 () Int)
(declare-fun _2@3@00 () Int)
(declare-fun result@4@00 () Int)
; ----- Well-definedness of specifications -----
(push) ; 1
(assert (= s@$ ($Snap.combine ($Snap.first s@$) ($Snap.second s@$))))
(assert (= ($Snap.first s@$) $Snap.unit))
(assert (= ($Snap.second s@$) $Snap.unit))
(declare-const $t@6@00 $Snap)
(assert (= $t@6@00 ($Snap.combine ($Snap.first $t@6@00) ($Snap.second $t@6@00))))
(assert (= ($Snap.first $t@6@00) $Snap.unit))
; [eval] result >= _1
(assert (>= result@4@00 _1@2@00))
(assert (=
  ($Snap.second $t@6@00)
  ($Snap.combine
    ($Snap.first ($Snap.second $t@6@00))
    ($Snap.second ($Snap.second $t@6@00)))))
(assert (= ($Snap.first ($Snap.second $t@6@00)) $Snap.unit))
; [eval] result >= _2
(assert (>= result@4@00 _2@3@00))
(assert (=
  ($Snap.second ($Snap.second $t@6@00))
  ($Snap.combine
    ($Snap.first ($Snap.second ($Snap.second $t@6@00)))
    ($Snap.second ($Snap.second ($Snap.second $t@6@00))))))
(assert (= ($Snap.first ($Snap.second ($Snap.second $t@6@00))) $Snap.unit))
; [eval] result == _1 || result == _2
; [eval] result == _1
(push) ; 2
; [then-branch: 0 | result@4@00 == _1@2@00 | live]
; [else-branch: 0 | result@4@00 != _1@2@00 | live]
(push) ; 3
; [then-branch: 0 | result@4@00 == _1@2@00]
(assert (= result@4@00 _1@2@00))
(pop) ; 3
(push) ; 3
; [else-branch: 0 | result@4@00 != _1@2@00]
(assert (not (= result@4@00 _1@2@00)))
; [eval] result == _2
(pop) ; 3
(pop) ; 2
; Joined path conditions
; Joined path conditions
(assert (or (= result@4@00 _1@2@00) (= result@4@00 _2@3@00)))
(assert (= ($Snap.second ($Snap.second ($Snap.second $t@6@00))) $Snap.unit))
; [eval] result == mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$(_1, _2)
; [eval] mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$(_1, _2)
(assert (=
  result@4@00
  (mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$<Int> _1@2@00 _2@3@00)))
(pop) ; 1
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (=
    (m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00)
    (m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  :pattern ((m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (m_max__$TY$__$int$$$int$$$int$%stateless _1@2@00 _2@3@00)
  :pattern ((m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (let ((result@4@00 (m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))) (and
    (and
      (and (>= result@4@00 _1@2@00) (>= result@4@00 _2@3@00))
      (or (= result@4@00 _1@2@00) (= result@4@00 _2@3@00)))
    (=
      result@4@00
      (mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$<Int> _1@2@00 _2@3@00))))
  :pattern ((m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))
  )))
; ----- Verification of function body and postcondition -----
(push) ; 1
(assert (= s@$ ($Snap.combine ($Snap.first s@$) ($Snap.second s@$))))
(assert (= ($Snap.first s@$) $Snap.unit))
(assert (= ($Snap.second s@$) $Snap.unit))
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [eval] (!(_1 > _2) ? _2 : _1)
; [eval] !(_1 > _2)
; [eval] _1 > _2
(push) ; 2
(set-option :timeout 10)
(push) ; 3
(assert (not (> _1@2@00 _2@3@00)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (not (> _1@2@00 _2@3@00))))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
; [then-branch: 1 | !(_1@2@00 > _2@3@00) | live]
; [else-branch: 1 | _1@2@00 > _2@3@00 | live]
(push) ; 3
; [then-branch: 1 | !(_1@2@00 > _2@3@00)]
(assert (not (> _1@2@00 _2@3@00)))
(pop) ; 3
(push) ; 3
; [else-branch: 1 | _1@2@00 > _2@3@00]
(assert (> _1@2@00 _2@3@00))
(pop) ; 3
(pop) ; 2
; Joined path conditions
; Joined path conditions
(assert (= result@4@00 (ite (not (> _1@2@00 _2@3@00)) _2@3@00 _1@2@00)))
; [eval] result >= _1
(set-option :timeout 10000)
(push) ; 2
(assert (not (>= result@4@00 _1@2@00)))
(check-sat)
; unsat
(pop) ; 2
; 0.00s
; (get-info :all-statistics)
(assert (>= result@4@00 _1@2@00))
; [eval] result >= _2
(push) ; 2
(assert (not (>= result@4@00 _2@3@00)))
(check-sat)
; unsat
(pop) ; 2
; 0.00s
; (get-info :all-statistics)
(assert (>= result@4@00 _2@3@00))
; [eval] result == _1 || result == _2
; [eval] result == _1
(push) ; 2
; [then-branch: 2 | result@4@00 == _1@2@00 | live]
; [else-branch: 2 | result@4@00 != _1@2@00 | live]
(push) ; 3
; [then-branch: 2 | result@4@00 == _1@2@00]
(assert (= result@4@00 _1@2@00))
(pop) ; 3
(push) ; 3
; [else-branch: 2 | result@4@00 != _1@2@00]
(assert (not (= result@4@00 _1@2@00)))
; [eval] result == _2
(pop) ; 3
(pop) ; 2
; Joined path conditions
; Joined path conditions
(push) ; 2
(assert (not (or (= result@4@00 _1@2@00) (= result@4@00 _2@3@00))))
(check-sat)
; unsat
(pop) ; 2
; 0.00s
; (get-info :all-statistics)
(assert (or (= result@4@00 _1@2@00) (= result@4@00 _2@3@00)))
(pop) ; 1
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (=
    (m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00)
    (ite (not (> _1@2@00 _2@3@00)) _2@3@00 _1@2@00))
  :pattern ((m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  )))
; ---------- DeadBorrowToken$ ----------
(declare-const borrow@7@00 Int)
; ---------- i32 ----------
(declare-const self@8@00 $Ref)
(push) ; 1
(declare-const $t@9@00 Int)
(assert (not (= self@8@00 $Ref.null)))
(pop) ; 1
; ---------- tuple0$ ----------
(declare-const self@10@00 $Ref)
(push) ; 1
(declare-const $t@11@00 $Snap)
(assert (= $t@11@00 $Snap.unit))
(pop) ; 1
