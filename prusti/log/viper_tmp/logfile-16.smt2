(get-info :version)
; (:version "4.8.6")
; Started: 2021-01-27 11:28:18
; Silicon.version: 1.1-SNAPSHOT (69b08d23+@(detached))
; Input file: dummy.vpr
; Verifier id: 00
; ------------------------------------------------------------
; Begin preamble
; ////////// Static preamble
; 
; ; /z3config.smt2
(set-option :print-success true) ; Boogie: false
(set-option :global-decls true) ; Boogie: default
(set-option :auto_config false) ; Usually a good idea
(set-option :smt.restart_strategy 0)
(set-option :smt.restart_factor |1.5|)
(set-option :smt.case_split 3)
(set-option :smt.delay_units true)
(set-option :smt.delay_units_threshold 16)
(set-option :nnf.sk_hack true)
(set-option :type_check true)
(set-option :smt.bv.reflect true)
(set-option :smt.mbqi false)
(set-option :smt.qi.eager_threshold 100)
(set-option :smt.qi.cost "(+ weight generation)")
(set-option :smt.qi.max_multi_patterns 1000)
(set-option :smt.phase_selection 0) ; default: 3, Boogie: 0
(set-option :sat.phase caching)
(set-option :sat.random_seed 0)
(set-option :nlsat.randomize true)
(set-option :nlsat.seed 0)
(set-option :nlsat.shuffle_vars false)
(set-option :fp.spacer.order_children 0) ; Not available with Z3 4.5
(set-option :fp.spacer.random_seed 0) ; Not available with Z3 4.5
(set-option :smt.arith.random_initial_value true) ; Boogie: true
(set-option :smt.random_seed 0)
(set-option :sls.random_offset true)
(set-option :sls.random_seed 0)
(set-option :sls.restart_init false)
(set-option :sls.walksat_ucb true)
(set-option :model.v2 true)
; 
; ; /preamble.smt2
(declare-datatypes () ((
    $Snap ($Snap.unit)
    ($Snap.combine ($Snap.first $Snap) ($Snap.second $Snap)))))
(declare-sort $Ref 0)
(declare-const $Ref.null $Ref)
(declare-sort $FPM)
(declare-sort $PPM)
(define-sort $Perm () Real)
(define-const $Perm.Write $Perm 1.0)
(define-const $Perm.No $Perm 0.0)
(define-fun $Perm.isValidVar ((p $Perm)) Bool
	(<= $Perm.No p))
(define-fun $Perm.isReadVar ((p $Perm) (ub $Perm)) Bool
    (and ($Perm.isValidVar p)
         (not (= p $Perm.No))
         (< p $Perm.Write)))
(define-fun $Perm.min ((p1 $Perm) (p2 $Perm)) Real
    (ite (<= p1 p2) p1 p2))
(define-fun $Math.min ((a Int) (b Int)) Int
    (ite (<= a b) a b))
(define-fun $Math.clip ((a Int)) Int
    (ite (< a 0) 0 a))
; ////////// Sorts
(declare-sort $SnapshotMirrors$)
(declare-sort $FVF<$Ref>)
; ////////// Sort wrappers
; Declaring additional sort wrappers
(declare-fun $SortWrappers.IntTo$Snap (Int) $Snap)
(declare-fun $SortWrappers.$SnapToInt ($Snap) Int)
(assert (forall ((x Int)) (!
    (= x ($SortWrappers.$SnapToInt($SortWrappers.IntTo$Snap x)))
    :pattern (($SortWrappers.IntTo$Snap x))
    :qid |$Snap.$SnapToIntTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.IntTo$Snap($SortWrappers.$SnapToInt x)))
    :pattern (($SortWrappers.$SnapToInt x))
    :qid |$Snap.IntTo$SnapToInt|
    )))
(declare-fun $SortWrappers.BoolTo$Snap (Bool) $Snap)
(declare-fun $SortWrappers.$SnapToBool ($Snap) Bool)
(assert (forall ((x Bool)) (!
    (= x ($SortWrappers.$SnapToBool($SortWrappers.BoolTo$Snap x)))
    :pattern (($SortWrappers.BoolTo$Snap x))
    :qid |$Snap.$SnapToBoolTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.BoolTo$Snap($SortWrappers.$SnapToBool x)))
    :pattern (($SortWrappers.$SnapToBool x))
    :qid |$Snap.BoolTo$SnapToBool|
    )))
(declare-fun $SortWrappers.$RefTo$Snap ($Ref) $Snap)
(declare-fun $SortWrappers.$SnapTo$Ref ($Snap) $Ref)
(assert (forall ((x $Ref)) (!
    (= x ($SortWrappers.$SnapTo$Ref($SortWrappers.$RefTo$Snap x)))
    :pattern (($SortWrappers.$RefTo$Snap x))
    :qid |$Snap.$SnapTo$RefTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$RefTo$Snap($SortWrappers.$SnapTo$Ref x)))
    :pattern (($SortWrappers.$SnapTo$Ref x))
    :qid |$Snap.$RefTo$SnapTo$Ref|
    )))
(declare-fun $SortWrappers.$PermTo$Snap ($Perm) $Snap)
(declare-fun $SortWrappers.$SnapTo$Perm ($Snap) $Perm)
(assert (forall ((x $Perm)) (!
    (= x ($SortWrappers.$SnapTo$Perm($SortWrappers.$PermTo$Snap x)))
    :pattern (($SortWrappers.$PermTo$Snap x))
    :qid |$Snap.$SnapTo$PermTo$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$PermTo$Snap($SortWrappers.$SnapTo$Perm x)))
    :pattern (($SortWrappers.$SnapTo$Perm x))
    :qid |$Snap.$PermTo$SnapTo$Perm|
    )))
; Declaring additional sort wrappers
(declare-fun $SortWrappers.$SnapshotMirrors$To$Snap ($SnapshotMirrors$) $Snap)
(declare-fun $SortWrappers.$SnapTo$SnapshotMirrors$ ($Snap) $SnapshotMirrors$)
(assert (forall ((x $SnapshotMirrors$)) (!
    (= x ($SortWrappers.$SnapTo$SnapshotMirrors$($SortWrappers.$SnapshotMirrors$To$Snap x)))
    :pattern (($SortWrappers.$SnapshotMirrors$To$Snap x))
    :qid |$Snap.$SnapTo$SnapshotMirrors$To$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$SnapshotMirrors$To$Snap($SortWrappers.$SnapTo$SnapshotMirrors$ x)))
    :pattern (($SortWrappers.$SnapTo$SnapshotMirrors$ x))
    :qid |$Snap.$SnapshotMirrors$To$SnapTo$SnapshotMirrors$|
    )))
; Declaring additional sort wrappers
(declare-fun $SortWrappers.$FVF<$Ref>To$Snap ($FVF<$Ref>) $Snap)
(declare-fun $SortWrappers.$SnapTo$FVF<$Ref> ($Snap) $FVF<$Ref>)
(assert (forall ((x $FVF<$Ref>)) (!
    (= x ($SortWrappers.$SnapTo$FVF<$Ref>($SortWrappers.$FVF<$Ref>To$Snap x)))
    :pattern (($SortWrappers.$FVF<$Ref>To$Snap x))
    :qid |$Snap.$SnapTo$FVF<$Ref>To$Snap|
    )))
(assert (forall ((x $Snap)) (!
    (= x ($SortWrappers.$FVF<$Ref>To$Snap($SortWrappers.$SnapTo$FVF<$Ref> x)))
    :pattern (($SortWrappers.$SnapTo$FVF<$Ref> x))
    :qid |$Snap.$FVF<$Ref>To$SnapTo$FVF<$Ref>|
    )))
; ////////// Symbols
(declare-fun mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$<Int> (Int Int) Int)
; Declaring symbols related to program functions (from program analysis)
(declare-fun builtin$unreach_bool__$TY$__$bool$ ($Snap) Bool)
(declare-fun builtin$unreach_bool__$TY$__$bool$%limited ($Snap) Bool)
(declare-const builtin$unreach_bool__$TY$__$bool$%stateless Bool)
(declare-fun read$ ($Snap) $Perm)
(declare-fun read$%limited ($Snap) $Perm)
(declare-const read$%stateless Bool)
(declare-fun m_max__$TY$__$int$$$int$$$int$ ($Snap Int Int) Int)
(declare-fun m_max__$TY$__$int$$$int$$$int$%limited ($Snap Int Int) Int)
(declare-fun m_max__$TY$__$int$$$int$$$int$%stateless (Int Int) Bool)
; Snapshot variable to be used during function verification
(declare-fun s@$ () $Snap)
; Declaring predicate trigger functions
(declare-fun DeadBorrowToken$%trigger ($Snap Int) Bool)
(declare-fun i32%trigger ($Snap $Ref) Bool)
(declare-fun tuple0$%trigger ($Snap $Ref) Bool)
; ////////// Uniqueness assumptions from domains
; ////////// Axioms
; End preamble
; ------------------------------------------------------------
; State saturation: after preamble
(set-option :timeout 100)
(check-sat)
; unknown
; ------------------------------------------------------------
; Begin function- and predicate-related preamble
; Declaring symbols related to program functions (from verification)
(assert (forall ((s@$ $Snap)) (!
  (=
    (builtin$unreach_bool__$TY$__$bool$%limited s@$)
    (builtin$unreach_bool__$TY$__$bool$ s@$))
  :pattern ((builtin$unreach_bool__$TY$__$bool$ s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (as builtin$unreach_bool__$TY$__$bool$%stateless  Bool)
  :pattern ((builtin$unreach_bool__$TY$__$bool$%limited s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (= (read$%limited s@$) (read$ s@$))
  :pattern ((read$ s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (as read$%stateless  Bool)
  :pattern ((read$%limited s@$))
  )))
(assert (forall ((s@$ $Snap)) (!
  (let ((result@1@00 (read$%limited s@$))) (and
    (< $Perm.No result@1@00)
    (< result@1@00 $Perm.Write)))
  :pattern ((read$%limited s@$))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (=
    (m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00)
    (m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  :pattern ((m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (m_max__$TY$__$int$$$int$$$int$%stateless _1@2@00 _2@3@00)
  :pattern ((m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (let ((result@4@00 (m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))) (and
    (and
      (and (>= result@4@00 _1@2@00) (>= result@4@00 _2@3@00))
      (or (= result@4@00 _1@2@00) (= result@4@00 _2@3@00)))
    (=
      result@4@00
      (mirror$m_max__$TY$__$int$$$int$$$int$__$TY$__$int$$$int$$$int$<Int> _1@2@00 _2@3@00))))
  :pattern ((m_max__$TY$__$int$$$int$$$int$%limited s@$ _1@2@00 _2@3@00))
  )))
(assert (forall ((s@$ $Snap) (_1@2@00 Int) (_2@3@00 Int)) (!
  (=
    (m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00)
    (ite (not (> _1@2@00 _2@3@00)) _2@3@00 _1@2@00))
  :pattern ((m_max__$TY$__$int$$$int$$$int$ s@$ _1@2@00 _2@3@00))
  )))
; End function- and predicate-related preamble
; ------------------------------------------------------------
; ---------- m_max3 ----------
(declare-const _0@0@16 $Ref)
(declare-const _0@1@16 $Ref)
(push) ; 1
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
(push) ; 2
(pop) ; 2
(push) ; 2
; [exec]
; var __t0: Bool
(declare-const __t0@2@16 Bool)
; [exec]
; var __t1: Bool
(declare-const __t1@3@16 Bool)
; [exec]
; var __t2: Bool
(declare-const __t2@4@16 Bool)
; [exec]
; var __t3: Bool
(declare-const __t3@5@16 Bool)
; [exec]
; var __t4: Bool
(declare-const __t4@6@16 Bool)
; [exec]
; var __t5: Bool
(declare-const __t5@7@16 Bool)
; [exec]
; var __t6: Bool
(declare-const __t6@8@16 Bool)
; [exec]
; var __t7: Bool
(declare-const __t7@9@16 Bool)
; [exec]
; var __t8: Bool
(declare-const __t8@10@16 Bool)
; [exec]
; var __t9: Bool
(declare-const __t9@11@16 Bool)
; [exec]
; var __t10: Bool
(declare-const __t10@12@16 Bool)
; [exec]
; var __t11: Bool
(declare-const __t11@13@16 Bool)
; [exec]
; var __t12: Bool
(declare-const __t12@14@16 Bool)
; [exec]
; var __t13: Bool
(declare-const __t13@15@16 Bool)
; [exec]
; var __t14: Bool
(declare-const __t14@16@16 Bool)
; [exec]
; var _1: Ref
(declare-const _1@17@16 $Ref)
; [exec]
; var _2: Ref
(declare-const _2@18@16 $Ref)
; [exec]
; var _3: Ref
(declare-const _3@19@16 $Ref)
; [exec]
; var _4: Ref
(declare-const _4@20@16 $Ref)
; [exec]
; var _5: Ref
(declare-const _5@21@16 $Ref)
; [exec]
; var _6: Int
(declare-const _6@22@16 Int)
; [exec]
; var _7: Int
(declare-const _7@23@16 Int)
; [exec]
; var _8: Ref
(declare-const _8@24@16 $Ref)
; [exec]
; var _9: Int
(declare-const _9@25@16 Int)
; [exec]
; var _10: Int
(declare-const _10@26@16 Int)
; [exec]
; var _11: Ref
(declare-const _11@27@16 $Ref)
; [exec]
; var _12: Int
(declare-const _12@28@16 Int)
; [exec]
; var _13: Int
(declare-const _13@29@16 Int)
; [exec]
; label start
; [exec]
; __t0 := false
; [exec]
; __t1 := false
; [exec]
; __t2 := false
; [exec]
; __t3 := false
; [exec]
; __t4 := false
; [exec]
; __t5 := false
; [exec]
; __t6 := false
; [exec]
; __t7 := false
; [exec]
; __t8 := false
; [exec]
; __t9 := false
; [exec]
; __t10 := false
; [exec]
; inhale acc(i32(_1), write) && (acc(i32(_2), write) && acc(i32(_3), write))
(declare-const $t@30@16 $Snap)
(assert (= $t@30@16 ($Snap.combine ($Snap.first $t@30@16) ($Snap.second $t@30@16))))
(assert (=
  ($Snap.second $t@30@16)
  ($Snap.combine
    ($Snap.first ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))))
(set-option :timeout 10)
(push) ; 3
(assert (not (= _1@17@16 _2@18@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (= _1@17@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (= _2@18@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@31@16 $Snap)
(assert (= $t@31@16 $Snap.unit))
; State saturation: after inhale
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@32@16 $Snap)
(assert (= $t@32@16 $Snap.unit))
; State saturation: after inhale
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@33@16 $Snap)
(assert (= $t@33@16 $Snap.unit))
; State saturation: after inhale
(check-sat)
; unknown
; [exec]
; label pre
; [exec]
; __t0 := true
; [exec]
; _6 := builtin$havoc_int()
(declare-const ret@34@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@35@16 $Snap)
(assert (= $t@35@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; unfold acc(i32(_1), write)
(define-fun mce_pTaken@36@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 3
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@36@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@36@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (= (- $Perm.Write (as mce_pTaken@36@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
(assert (not (= _1@17@16 $Ref.null)))
; State saturation: after unfold
(set-option :timeout 40)
(check-sat)
; unknown
(assert (i32%trigger ($Snap.first $t@30@16) _1@17@16))
; [exec]
; _6 := _1.val_int
(declare-const _6@37@16 Int)
(assert (= _6@37@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
; [exec]
; label l0
; [exec]
; _7 := builtin$havoc_int()
(declare-const ret@38@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@39@16 $Snap)
(assert (= $t@39@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; unfold acc(i32(_2), write)
(define-fun mce_pTaken@40@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 3
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@40@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@40@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (= (- $Perm.Write (as mce_pTaken@40@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))))
(set-option :timeout 10)
(push) ; 3
(assert (not (= _1@17@16 _2@18@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(assert (not (= _2@18@16 $Ref.null)))
; State saturation: after unfold
(set-option :timeout 40)
(check-sat)
; unknown
(assert (i32%trigger ($Snap.first ($Snap.second $t@30@16)) _2@18@16))
; [exec]
; _7 := _2.val_int
(assert (and
  (=
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))))
(set-option :timeout 10000)
(push) ; 3
(assert (not (< $Perm.No (+ $Perm.Write (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(declare-const _7@41@16 Int)
(assert (= _7@41@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
; [exec]
; label l1
; [exec]
; _5 := builtin$havoc_ref()
(declare-const ret@42@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_5.val_bool, write)
(declare-const $t@43@16 Bool)
(assert (not (= ret@42@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _5.val_bool := _6 > _7
; [eval] _6 > _7
(define-fun mce_pTaken@44@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 3
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@44@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@44@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (= (- $Perm.Write (as mce_pTaken@44@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(declare-const val_bool@45@16 Bool)
(assert (= val_bool@45@16 (> _6@37@16 _7@41@16)))
; [exec]
; __t11 := _5.val_bool
; [eval] !__t11
(set-option :timeout 10)
(push) ; 3
(assert (not val_bool@45@16))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not (not val_bool@45@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
; [then-branch: 0 | !(val_bool@45@16) | live]
; [else-branch: 0 | val_bool@45@16 | live]
(push) ; 3
; [then-branch: 0 | !(val_bool@45@16)]
(assert (not val_bool@45@16))
; [exec]
; label l3
; [exec]
; unfold acc(i32(_3), write)
(define-fun mce_pTaken@46@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 4
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@46@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@46@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= (- $Perm.Write (as mce_pTaken@46@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 4
(assert (not (= _1@17@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= _2@18@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (not (= _3@19@16 $Ref.null)))
; State saturation: after unfold
(set-option :timeout 40)
(check-sat)
; unknown
(assert (i32%trigger ($Snap.second ($Snap.second $t@30@16)) _3@19@16))
; [exec]
; label bb3
; [exec]
; __t3 := true
; [exec]
; _4 := builtin$havoc_ref()
(declare-const ret@47@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_4.val_bool, write)
(declare-const $t@48@16 Bool)
(set-option :timeout 10)
(push) ; 4
(assert (not (= ret@42@16 ret@47@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@47@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _4.val_bool := false
(define-fun mce_pTaken@49@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 4
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@49@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@49@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= (- $Perm.Write (as mce_pTaken@49@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@48@16 $t@48@16)
  (implies (= ret@42@16 ret@47@16) (= $t@48@16 val_bool@45@16))))
(set-option :timeout 10)
(push) ; 4
(assert (not (= ret@42@16 ret@47@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l6
; [exec]
; __t4 := true
; [exec]
; __t13 := _4.val_bool
(assert (and (= false false) (implies (= ret@42@16 ret@47@16) (= false val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 4
(assert (not (< $Perm.No (+ $Perm.Write (ite (= ret@42@16 ret@47@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [then-branch: 1 | False | dead]
; [else-branch: 1 | True | live]
(push) ; 4
; [else-branch: 1 | True]
(pop) ; 4
; [eval] !__t13
(set-option :timeout 10)
(push) ; 4
(assert (not false))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [then-branch: 2 | True | live]
; [else-branch: 2 | False | dead]
(push) ; 4
; [then-branch: 2 | True]
; [exec]
; label l7
; [exec]
; __t6 := true
; [exec]
; _12 := builtin$havoc_int()
(declare-const ret@50@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@51@16 $Snap)
(assert (= $t@51@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _12 := _2.val_int
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))))))
(set-option :timeout 10000)
(push) ; 5
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(declare-const _12@52@16 Int)
(assert (= _12@52@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
; [exec]
; label l11
; [exec]
; _13 := builtin$havoc_int()
(declare-const ret@53@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@54@16 $Snap)
(assert (= $t@54@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _13 := _3.val_int
(assert (and
  (=
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 5
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(declare-const _13@55@16 Int)
(assert (= _13@55@16 ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
; [exec]
; label l12
; [exec]
; _11 := builtin$havoc_ref()
(declare-const ret@56@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_11.val_bool, write)
(declare-const $t@57@16 Bool)
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@42@16 ret@56@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@47@16 ret@56@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@56@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _11.val_bool := _12 > _13
; [eval] _12 > _13
(define-fun mce_pTaken@58@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 5
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@58@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@58@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= (- $Perm.Write (as mce_pTaken@58@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@57@16 $t@57@16)
  (implies (= ret@42@16 ret@56@16) (= $t@57@16 val_bool@45@16))
  (implies (= ret@47@16 ret@56@16) (= $t@57@16 false))))
(declare-const val_bool@59@16 Bool)
(assert (= val_bool@59@16 (> _12@52@16 _13@55@16)))
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@47@16 ret@56@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@42@16 ret@56@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [exec]
; __t14 := _11.val_bool
(assert (and
  (= val_bool@59@16 val_bool@59@16)
  (implies (= ret@47@16 ret@56@16) (= val_bool@59@16 false))
  (implies (= ret@42@16 ret@56@16) (= val_bool@59@16 val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 5
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= ret@47@16 ret@56@16) $Perm.Write $Perm.No))
    (ite (= ret@42@16 ret@56@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [eval] !__t14
(set-option :timeout 10)
(push) ; 5
(assert (not val_bool@59@16))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (not val_bool@59@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [then-branch: 3 | !(val_bool@59@16) | live]
; [else-branch: 3 | val_bool@59@16 | live]
(push) ; 5
; [then-branch: 3 | !(val_bool@59@16)]
(assert (not val_bool@59@16))
; [exec]
; label bb2
; [exec]
; __t8 := true
; [exec]
; _0 := builtin$havoc_ref()
(declare-const ret@60@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_0.val_int, write)
(declare-const $t@61@16 Int)
(set-option :timeout 10)
(push) ; 6
(assert (not (= _2@18@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _3@19@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@60@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _0.val_int := _3.val_int
(assert (and
  (implies
    (= ret@60@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      $t@61@16))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+
      (+
        (ite (= ret@60@16 _3@19@16) $Perm.Write $Perm.No)
        (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(define-fun mce_pTaken@62@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@62@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@62@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@62@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@61@16 $t@61@16)
  (implies
    (= _2@18@16 ret@60@16)
    (= $t@61@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@60@16)
    (= $t@61@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _3@19@16 ret@60@16)
    (=
      $t@61@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(declare-const val_int@63@16 Int)
(assert (=
  val_int@63@16
  ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
(set-option :timeout 10)
(push) ; 6
(assert (not (= _3@19@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _2@18@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l16
; [exec]
; label bb4
; [exec]
; __t9 := true
; [exec]
; label l9
; [exec]
; __t10 := true
; [exec]
; label l18
; [exec]
; fold acc(i32(_0), write)
(define-fun mce_pTaken@64@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@64@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@64@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@64@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@63@16 val_int@63@16)
  (implies
    (= _3@19@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@60@16)
    (= val_int@63@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(assert (i32%trigger ($SortWrappers.IntTo$Snap val_int@63@16) ret@60@16))
; [exec]
; assert (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int)
(push) ; 6
(define-fun mce_pTaken@65@16 () $Perm
  $Perm.Write)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@65@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@65@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@65@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 7
(assert (not (= _3@19@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _1@17@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _2@18@16 ret@60@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@63@16 val_int@63@16)
  (implies
    (= _3@19@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@60@16)
    (= val_int@63@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (+ $Perm.Write (ite (= _3@19@16 ret@60@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 ret@60@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 ret@60@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (= val_int@63@16 val_int@63@16)
  (implies
    (= _3@19@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@60@16)
    (= val_int@63@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@60@16)
    (=
      val_int@63@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] old[pre]((unfolding acc(i32(_1), write) in _1.val_int))
; [eval] (unfolding acc(i32(_1), write) in _1.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ (ite (= _3@19@16 _1@17@16) $Perm.Write $Perm.No) $Perm.Write)
    (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int)))
; [eval] old[pre]((unfolding acc(i32(_2), write) in _2.val_int))
; [eval] (unfolding acc(i32(_2), write) in _2.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
; [eval] old[pre]((unfolding acc(i32(_3), write) in _3.val_int))
; [eval] (unfolding acc(i32(_3), write) in _3.val_int)
(push) ; 6
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(assert (not (=
  val_int@63@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (=
  val_int@63@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
; [exec]
; assert true
; [exec]
; exhale acc(i32(_0), write)
(define-fun mce_pTaken@66@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@66@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@66@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@66@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label end_of_method
(pop) ; 5
(push) ; 5
; [else-branch: 3 | val_bool@59@16]
(assert val_bool@59@16)
(pop) ; 5
; [eval] !!__t14
; [eval] !__t14
(set-option :timeout 10)
(push) ; 5
(assert (not (not val_bool@59@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not val_bool@59@16))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [then-branch: 4 | val_bool@59@16 | live]
; [else-branch: 4 | !(val_bool@59@16) | live]
(push) ; 5
; [then-branch: 4 | val_bool@59@16]
(assert val_bool@59@16)
; [exec]
; label bb1
; [exec]
; __t7 := true
; [exec]
; _0 := builtin$havoc_ref()
(declare-const ret@67@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_0.val_int, write)
(declare-const $t@68@16 Int)
(set-option :timeout 10)
(push) ; 6
(assert (not (= _2@18@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _3@19@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@67@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _0.val_int := _2.val_int
(assert (and
  (implies
    (= ret@67@16 _2@18@16)
    (= ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) $t@68@16))
  (=
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+
      (+ (ite (= ret@67@16 _2@18@16) $Perm.Write $Perm.No) $Perm.Write)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(define-fun mce_pTaken@69@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@69@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@69@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@69@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@68@16 $t@68@16)
  (implies
    (= _2@18@16 ret@67@16)
    (= $t@68@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@67@16)
    (= $t@68@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _3@19@16 ret@67@16)
    (=
      $t@68@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(declare-const val_int@70@16 Int)
(assert (=
  val_int@70@16
  ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
(set-option :timeout 10)
(push) ; 6
(assert (not (= _3@19@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _2@18@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l15
; [exec]
; label bb4
; [exec]
; __t9 := true
; [exec]
; label l9
; [exec]
; __t10 := true
; [exec]
; label l18
; [exec]
; fold acc(i32(_0), write)
(define-fun mce_pTaken@71@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@71@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@71@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@71@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@70@16 val_int@70@16)
  (implies
    (= _3@19@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@67@16)
    (= val_int@70@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(assert (i32%trigger ($SortWrappers.IntTo$Snap val_int@70@16) ret@67@16))
; [exec]
; assert (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int)
(push) ; 6
(define-fun mce_pTaken@72@16 () $Perm
  $Perm.Write)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@72@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@72@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@72@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 7
(assert (not (= _3@19@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _1@17@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _2@18@16 ret@67@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@70@16 val_int@70@16)
  (implies
    (= _3@19@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@67@16)
    (= val_int@70@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (+ $Perm.Write (ite (= _3@19@16 ret@67@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 ret@67@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 ret@67@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (= val_int@70@16 val_int@70@16)
  (implies
    (= _3@19@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@67@16)
    (= val_int@70@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@67@16)
    (=
      val_int@70@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] old[pre]((unfolding acc(i32(_1), write) in _1.val_int))
; [eval] (unfolding acc(i32(_1), write) in _1.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ (ite (= _3@19@16 _1@17@16) $Perm.Write $Perm.No) $Perm.Write)
    (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int)))
; [eval] old[pre]((unfolding acc(i32(_2), write) in _2.val_int))
; [eval] (unfolding acc(i32(_2), write) in _2.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
; [eval] old[pre]((unfolding acc(i32(_3), write) in _3.val_int))
; [eval] (unfolding acc(i32(_3), write) in _3.val_int)
(push) ; 6
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(assert (not (=
  val_int@70@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (=
  val_int@70@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
; [exec]
; assert true
; [exec]
; exhale acc(i32(_0), write)
(define-fun mce_pTaken@73@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@73@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@73@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@73@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label end_of_method
(pop) ; 5
(push) ; 5
; [else-branch: 4 | !(val_bool@59@16)]
(assert (not val_bool@59@16))
(pop) ; 5
(pop) ; 4
(pop) ; 3
(push) ; 3
; [else-branch: 0 | val_bool@45@16]
(assert val_bool@45@16)
(pop) ; 3
; [eval] !!__t11
; [eval] !__t11
(set-option :timeout 10)
(push) ; 3
(assert (not (not val_bool@45@16)))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
(push) ; 3
(assert (not val_bool@45@16))
(check-sat)
; unknown
(pop) ; 3
; 0.00s
; (get-info :all-statistics)
; [then-branch: 5 | val_bool@45@16 | live]
; [else-branch: 5 | !(val_bool@45@16) | live]
(push) ; 3
; [then-branch: 5 | val_bool@45@16]
(assert val_bool@45@16)
; [exec]
; label return
; [exec]
; __t1 := true
; [exec]
; _9 := builtin$havoc_int()
(declare-const ret@74@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@75@16 $Snap)
(assert (= $t@75@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _9 := _1.val_int
(assert (and
  (implies
    (= _2@18@16 _1@17@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))
    ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)))))
(set-option :timeout 10000)
(push) ; 4
(assert (not (< $Perm.No (+ (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No) $Perm.Write))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(declare-const _9@76@16 Int)
(assert (= _9@76@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
; [exec]
; label l4
; [exec]
; _10 := builtin$havoc_int()
(declare-const ret@77@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@78@16 $Snap)
(assert (= $t@78@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; unfold acc(i32(_3), write)
(define-fun mce_pTaken@79@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 4
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@79@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@79@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= (- $Perm.Write (as mce_pTaken@79@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 4
(assert (not (= _1@17@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= _2@18@16 _3@19@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (not (= _3@19@16 $Ref.null)))
; State saturation: after unfold
(set-option :timeout 40)
(check-sat)
; unknown
(assert (i32%trigger ($Snap.second ($Snap.second $t@30@16)) _3@19@16))
; [exec]
; _10 := _3.val_int
(assert (and
  (=
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 4
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(declare-const _10@80@16 Int)
(assert (= _10@80@16 ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
; [exec]
; label l5
; [exec]
; _8 := builtin$havoc_ref()
(declare-const ret@81@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_8.val_bool, write)
(declare-const $t@82@16 Bool)
(set-option :timeout 10)
(push) ; 4
(assert (not (= ret@42@16 ret@81@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@81@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _8.val_bool := _9 > _10
; [eval] _9 > _10
(define-fun mce_pTaken@83@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 4
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@83@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@83@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (= (- $Perm.Write (as mce_pTaken@83@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@82@16 $t@82@16)
  (implies (= ret@42@16 ret@81@16) (= $t@82@16 val_bool@45@16))))
(declare-const val_bool@84@16 Bool)
(assert (= val_bool@84@16 (> _9@76@16 _10@80@16)))
(set-option :timeout 10)
(push) ; 4
(assert (not (= ret@42@16 ret@81@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [exec]
; __t12 := _8.val_bool
(assert (and
  (= val_bool@84@16 val_bool@84@16)
  (implies (= ret@42@16 ret@81@16) (= val_bool@84@16 val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 4
(assert (not (< $Perm.No (+ $Perm.Write (ite (= ret@42@16 ret@81@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [eval] !__t12
(set-option :timeout 10)
(push) ; 4
(assert (not val_bool@84@16))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not (not val_bool@84@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [then-branch: 6 | !(val_bool@84@16) | live]
; [else-branch: 6 | val_bool@84@16 | live]
(push) ; 4
; [then-branch: 6 | !(val_bool@84@16)]
(assert (not val_bool@84@16))
; [exec]
; label l2
; [exec]
; label bb3
; [exec]
; __t3 := true
; [exec]
; _4 := builtin$havoc_ref()
(declare-const ret@85@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_4.val_bool, write)
(declare-const $t@86@16 Bool)
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@42@16 ret@85@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@81@16 ret@85@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@85@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _4.val_bool := false
(define-fun mce_pTaken@87@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 5
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@87@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@87@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= (- $Perm.Write (as mce_pTaken@87@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@86@16 $t@86@16)
  (implies (= ret@42@16 ret@85@16) (= $t@86@16 val_bool@45@16))
  (implies (= ret@81@16 ret@85@16) (= $t@86@16 val_bool@84@16))))
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@81@16 ret@85@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@42@16 ret@85@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l6
; [exec]
; __t4 := true
; [exec]
; __t13 := _4.val_bool
(assert (and
  (= false false)
  (implies (= ret@81@16 ret@85@16) (= false val_bool@84@16))
  (implies (= ret@42@16 ret@85@16) (= false val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 5
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= ret@81@16 ret@85@16) $Perm.Write $Perm.No))
    (ite (= ret@42@16 ret@85@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [then-branch: 7 | False | dead]
; [else-branch: 7 | True | live]
(push) ; 5
; [else-branch: 7 | True]
(pop) ; 5
; [eval] !__t13
(set-option :timeout 10)
(push) ; 5
(assert (not false))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [then-branch: 8 | True | live]
; [else-branch: 8 | False | dead]
(push) ; 5
; [then-branch: 8 | True]
; [exec]
; label l7
; [exec]
; __t6 := true
; [exec]
; _12 := builtin$havoc_int()
(declare-const ret@88@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@89@16 $Snap)
(assert (= $t@89@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _12 := _2.val_int
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 _2@18@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(declare-const _12@90@16 Int)
(assert (= _12@90@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
; [exec]
; label l11
; [exec]
; _13 := builtin$havoc_int()
(declare-const ret@91@16 Int)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale true
(declare-const $t@92@16 $Snap)
(assert (= $t@92@16 $Snap.unit))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _13 := _3.val_int
(assert (and
  (=
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(declare-const _13@93@16 Int)
(assert (= _13@93@16 ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
; [exec]
; label l12
; [exec]
; _11 := builtin$havoc_ref()
(declare-const ret@94@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_11.val_bool, write)
(declare-const $t@95@16 Bool)
(set-option :timeout 10)
(push) ; 6
(assert (not (= ret@42@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= ret@81@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= ret@85@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@94@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _11.val_bool := _12 > _13
; [eval] _12 > _13
(define-fun mce_pTaken@96@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@96@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@96@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@96@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@95@16 $t@95@16)
  (implies (= ret@42@16 ret@94@16) (= $t@95@16 val_bool@45@16))
  (implies (= ret@81@16 ret@94@16) (= $t@95@16 val_bool@84@16))
  (implies (= ret@85@16 ret@94@16) (= $t@95@16 false))))
(declare-const val_bool@97@16 Bool)
(assert (= val_bool@97@16 (> _12@90@16 _13@93@16)))
(set-option :timeout 10)
(push) ; 6
(assert (not (= ret@85@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= ret@81@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= ret@42@16 ret@94@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; __t14 := _11.val_bool
(assert (and
  (= val_bool@97@16 val_bool@97@16)
  (implies (= ret@85@16 ret@94@16) (= val_bool@97@16 false))
  (implies (= ret@81@16 ret@94@16) (= val_bool@97@16 val_bool@84@16))
  (implies (= ret@42@16 ret@94@16) (= val_bool@97@16 val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+
      (+ $Perm.Write (ite (= ret@85@16 ret@94@16) $Perm.Write $Perm.No))
      (ite (= ret@81@16 ret@94@16) $Perm.Write $Perm.No))
    (ite (= ret@42@16 ret@94@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [eval] !__t14
(set-option :timeout 10)
(push) ; 6
(assert (not val_bool@97@16))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (not val_bool@97@16)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [then-branch: 9 | !(val_bool@97@16) | live]
; [else-branch: 9 | val_bool@97@16 | dead]
(push) ; 6
; [then-branch: 9 | !(val_bool@97@16)]
(assert (not val_bool@97@16))
; [exec]
; label bb2
; [exec]
; __t8 := true
; [exec]
; _0 := builtin$havoc_ref()
(declare-const ret@98@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_0.val_int, write)
(declare-const $t@99@16 Int)
(set-option :timeout 10)
(push) ; 7
(assert (not (= _2@18@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _1@17@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _3@19@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@98@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _0.val_int := _3.val_int
(assert (and
  (implies
    (= ret@98@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      $t@99@16))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 _3@19@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))
    ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))
(set-option :timeout 10000)
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (+
        (ite (= ret@98@16 _3@19@16) $Perm.Write $Perm.No)
        (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(define-fun mce_pTaken@100@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@100@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@100@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@100@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@99@16 $t@99@16)
  (implies
    (= _2@18@16 ret@98@16)
    (= $t@99@16 ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@98@16)
    (= $t@99@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _3@19@16 ret@98@16)
    (=
      $t@99@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(declare-const val_int@101@16 Int)
(assert (=
  val_int@101@16
  ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
(set-option :timeout 10)
(push) ; 7
(assert (not (= _3@19@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _1@17@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _2@18@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l16
; [exec]
; label bb4
; [exec]
; __t9 := true
; [exec]
; label l9
; [exec]
; __t10 := true
; [exec]
; label l18
; [exec]
; fold acc(i32(_0), write)
(define-fun mce_pTaken@102@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@102@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@102@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@102@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@101@16 val_int@101@16)
  (implies
    (= _3@19@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@98@16)
    (= val_int@101@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(assert (i32%trigger ($SortWrappers.IntTo$Snap val_int@101@16) ret@98@16))
; [exec]
; assert (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int)
(push) ; 7
(define-fun mce_pTaken@103@16 () $Perm
  $Perm.Write)
(push) ; 8
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@103@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@103@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(push) ; 8
(assert (not (= (- $Perm.Write (as mce_pTaken@103@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 8
(assert (not (= _3@19@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(push) ; 8
(assert (not (= _1@17@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(push) ; 8
(assert (not (= _2@18@16 ret@98@16)))
(check-sat)
; unknown
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@101@16 val_int@101@16)
  (implies
    (= _3@19@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@98@16)
    (= val_int@101@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 8
(assert (not (<
  $Perm.No
  (+
    (+
      (+ $Perm.Write (ite (= _3@19@16 ret@98@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 ret@98@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 ret@98@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(pop) ; 7
; Joined path conditions
(assert (and
  (= val_int@101@16 val_int@101@16)
  (implies
    (= _3@19@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@98@16)
    (= val_int@101@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@98@16)
    (=
      val_int@101@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] old[pre]((unfolding acc(i32(_1), write) in _1.val_int))
; [eval] (unfolding acc(i32(_1), write) in _1.val_int)
(push) ; 7
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 8
(assert (not (<
  $Perm.No
  (+
    (+ (ite (= _3@19@16 _1@17@16) $Perm.Write $Perm.No) $Perm.Write)
    (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(pop) ; 7
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int)))
; [eval] old[pre]((unfolding acc(i32(_2), write) in _2.val_int))
; [eval] (unfolding acc(i32(_2), write) in _2.val_int)
(push) ; 7
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
(push) ; 8
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(pop) ; 7
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
; [eval] old[pre]((unfolding acc(i32(_3), write) in _3.val_int))
; [eval] (unfolding acc(i32(_3), write) in _3.val_int)
(push) ; 7
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 8
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 8
; 0.00s
; (get-info :all-statistics)
(pop) ; 7
; Joined path conditions
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(pop) ; 7
; Joined path conditions
(push) ; 7
(pop) ; 7
; Joined path conditions
(push) ; 7
(assert (not (=
  val_int@101@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (=
  val_int@101@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
; [exec]
; assert true
; [exec]
; exhale acc(i32(_0), write)
(define-fun mce_pTaken@104@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@104@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@104@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@104@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
; [exec]
; label end_of_method
(pop) ; 6
; [eval] !!__t14
; [eval] !__t14
(set-option :timeout 10)
(push) ; 6
(assert (not (not val_bool@97@16)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [then-branch: 10 | val_bool@97@16 | dead]
; [else-branch: 10 | !(val_bool@97@16) | live]
(push) ; 6
; [else-branch: 10 | !(val_bool@97@16)]
(assert (not val_bool@97@16))
(pop) ; 6
(pop) ; 5
(pop) ; 4
(push) ; 4
; [else-branch: 6 | val_bool@84@16]
(assert val_bool@84@16)
(pop) ; 4
; [eval] !!__t12
; [eval] !__t12
(push) ; 4
(assert (not (not val_bool@84@16)))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
(push) ; 4
(assert (not val_bool@84@16))
(check-sat)
; unknown
(pop) ; 4
; 0.00s
; (get-info :all-statistics)
; [then-branch: 11 | val_bool@84@16 | live]
; [else-branch: 11 | !(val_bool@84@16) | live]
(push) ; 4
; [then-branch: 11 | val_bool@84@16]
(assert val_bool@84@16)
; [exec]
; label bb0
; [exec]
; __t2 := true
; [exec]
; _4 := builtin$havoc_ref()
(declare-const ret@105@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_4.val_bool, write)
(declare-const $t@106@16 Bool)
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@42@16 ret@105@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@81@16 ret@105@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@105@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _4.val_bool := true
(define-fun mce_pTaken@107@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 5
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@107@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@107@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= (- $Perm.Write (as mce_pTaken@107@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@106@16 $t@106@16)
  (implies (= ret@42@16 ret@105@16) (= $t@106@16 val_bool@45@16))
  (implies (= ret@81@16 ret@105@16) (= $t@106@16 val_bool@84@16))))
(set-option :timeout 10)
(push) ; 5
(assert (not (= ret@81@16 ret@105@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(push) ; 5
(assert (not (= ret@42@16 ret@105@16)))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l6
; [exec]
; __t4 := true
; [exec]
; __t13 := _4.val_bool
(assert (and
  (= true true)
  (implies (= ret@81@16 ret@105@16) (= true val_bool@84@16))
  (implies (= ret@42@16 ret@105@16) (= true val_bool@45@16))))
(set-option :timeout 10000)
(push) ; 5
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= ret@81@16 ret@105@16) $Perm.Write $Perm.No))
    (ite (= ret@42@16 ret@105@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 5
(assert (not false))
(check-sat)
; unknown
(pop) ; 5
; 0.00s
; (get-info :all-statistics)
; [then-branch: 12 | True | live]
; [else-branch: 12 | False | dead]
(push) ; 5
; [then-branch: 12 | True]
; [exec]
; label l8
; [exec]
; __t5 := true
; [exec]
; _0 := builtin$havoc_ref()
(declare-const ret@108@16 $Ref)
; State saturation: after contract
(set-option :timeout 50)
(check-sat)
; unknown
; [exec]
; inhale acc(_0.val_int, write)
(declare-const $t@109@16 Int)
(set-option :timeout 10)
(push) ; 6
(assert (not (= _2@18@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _3@19@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (not (= ret@108@16 $Ref.null)))
; State saturation: after inhale
(set-option :timeout 20)
(check-sat)
; unknown
; [exec]
; _0.val_int := _1.val_int
(assert (and
  (implies
    (= ret@108@16 _1@17@16)
    (= ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) $t@109@16))
  (implies
    (= _2@18@16 _1@17@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (=
    ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))
    ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)))
  (implies
    (= _3@19@16 _1@17@16)
    (=
      ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 6
(assert (not (<
  $Perm.No
  (+
    (+
      (+
        (ite (= ret@108@16 _1@17@16) $Perm.Write $Perm.No)
        (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No))
      $Perm.Write)
    (ite (= _3@19@16 _1@17@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(define-fun mce_pTaken@110@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@110@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@110@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@110@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= $t@109@16 $t@109@16)
  (implies
    (= _2@18@16 ret@108@16)
    (=
      $t@109@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@108@16)
    (= $t@109@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _3@19@16 ret@108@16)
    (=
      $t@109@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
(declare-const val_int@111@16 Int)
(assert (= val_int@111@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
(set-option :timeout 10)
(push) ; 6
(assert (not (= _3@19@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _1@17@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= _2@18@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label l10
; [exec]
; label l9
; [exec]
; __t10 := true
; [exec]
; label l18
; [exec]
; fold acc(i32(_0), write)
(define-fun mce_pTaken@112@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@112@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@112@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@112@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@111@16 val_int@111@16)
  (implies
    (= _3@19@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@108@16)
    (= val_int@111@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(assert (i32%trigger ($SortWrappers.IntTo$Snap val_int@111@16) ret@108@16))
; [exec]
; assert (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int) == m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] (unfolding acc(i32(_0), write) in _0.val_int)
(push) ; 6
(define-fun mce_pTaken@113@16 () $Perm
  $Perm.Write)
(push) ; 7
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@113@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@113@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= (- $Perm.Write (as mce_pTaken@113@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(set-option :timeout 10)
(push) ; 7
(assert (not (= _3@19@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _1@17@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(push) ; 7
(assert (not (= _2@18@16 ret@108@16)))
(check-sat)
; unknown
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(assert (and
  (= val_int@111@16 val_int@111@16)
  (implies
    (= _3@19@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@108@16)
    (= val_int@111@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
(set-option :timeout 10000)
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (+ $Perm.Write (ite (= _3@19@16 ret@108@16) $Perm.Write $Perm.No))
      (ite (= _1@17@16 ret@108@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 ret@108@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (= val_int@111@16 val_int@111@16)
  (implies
    (= _3@19@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))
  (implies
    (= _1@17@16 ret@108@16)
    (= val_int@111@16 ($SortWrappers.$SnapToInt ($Snap.first $t@30@16))))
  (implies
    (= _2@18@16 ret@108@16)
    (=
      val_int@111@16
      ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16)))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_1), write) in _1.val_int)), m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int))))
; [eval] old[pre]((unfolding acc(i32(_1), write) in _1.val_int))
; [eval] (unfolding acc(i32(_1), write) in _1.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ (ite (= _3@19@16 _1@17@16) $Perm.Write $Perm.No) $Perm.Write)
    (ite (= _2@18@16 _1@17@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.second ($Snap.second $t@30@16))))
  (= ($Snap.first $t@30@16) ($Snap.first $t@30@16))
  (implies
    (= _2@18@16 _1@17@16)
    (= ($Snap.first $t@30@16) ($Snap.first ($Snap.second $t@30@16))))))
; [eval] m_max__$TY$__$int$$$int$$$int$(old[pre]((unfolding acc(i32(_2), write) in _2.val_int)), old[pre]((unfolding acc(i32(_3), write) in _3.val_int)))
; [eval] old[pre]((unfolding acc(i32(_2), write) in _2.val_int))
; [eval] (unfolding acc(i32(_2), write) in _2.val_int)
(push) ; 6
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+
      (ite (= _3@19@16 _2@18@16) $Perm.Write $Perm.No)
      (ite (= _1@17@16 _2@18@16) $Perm.Write $Perm.No))
    $Perm.Write))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (implies
    (= _3@19@16 _2@18@16)
    (=
      ($Snap.first ($Snap.second $t@30@16))
      ($Snap.second ($Snap.second $t@30@16))))
  (implies
    (= _1@17@16 _2@18@16)
    (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (= ($Snap.first ($Snap.second $t@30@16)) ($Snap.first ($Snap.second $t@30@16)))))
; [eval] old[pre]((unfolding acc(i32(_3), write) in _3.val_int))
; [eval] (unfolding acc(i32(_3), write) in _3.val_int)
(push) ; 6
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 7
(assert (not (<
  $Perm.No
  (+
    (+ $Perm.Write (ite (= _1@17@16 _3@19@16) $Perm.Write $Perm.No))
    (ite (= _2@18@16 _3@19@16) $Perm.Write $Perm.No)))))
(check-sat)
; unsat
(pop) ; 7
; 0.00s
; (get-info :all-statistics)
(pop) ; 6
; Joined path conditions
(assert (and
  (=
    ($Snap.second ($Snap.second $t@30@16))
    ($Snap.second ($Snap.second $t@30@16)))
  (implies
    (= _1@17@16 _3@19@16)
    (= ($Snap.second ($Snap.second $t@30@16)) ($Snap.first $t@30@16)))
  (implies
    (= _2@18@16 _3@19@16)
    (=
      ($Snap.second ($Snap.second $t@30@16))
      ($Snap.first ($Snap.second $t@30@16))))))
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(pop) ; 6
; Joined path conditions
(push) ; 6
(assert (not (=
  val_int@111@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16))))))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(assert (=
  val_int@111@16
  (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine $Snap.unit $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first $t@30@16)) (m_max__$TY$__$int$$$int$$$int$ ($Snap.combine
    $Snap.unit
    $Snap.unit) ($SortWrappers.$SnapToInt ($Snap.first ($Snap.second $t@30@16))) ($SortWrappers.$SnapToInt ($Snap.second ($Snap.second $t@30@16)))))))
; [exec]
; assert true
; [exec]
; exhale acc(i32(_0), write)
(define-fun mce_pTaken@114@16 () $Perm
  $Perm.Write)
(set-option :timeout 500)
(push) ; 6
(assert (not (or
  (= (- $Perm.Write (as mce_pTaken@114@16  $Perm)) $Perm.No)
  (< (- $Perm.Write (as mce_pTaken@114@16  $Perm)) $Perm.No))))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
(push) ; 6
(assert (not (= (- $Perm.Write (as mce_pTaken@114@16  $Perm)) $Perm.No)))
(check-sat)
; unsat
(pop) ; 6
; 0.00s
; (get-info :all-statistics)
; [exec]
; label end_of_method
(pop) ; 5
; [eval] !__t13
; [then-branch: 13 | False | dead]
; [else-branch: 13 | True | live]
(push) ; 5
; [else-branch: 13 | True]
(pop) ; 5
(pop) ; 4
(push) ; 4
; [else-branch: 11 | !(val_bool@84@16)]
(assert (not val_bool@84@16))
(pop) ; 4
(pop) ; 3
(push) ; 3
; [else-branch: 5 | !(val_bool@45@16)]
(assert (not val_bool@45@16))
(pop) ; 3
(pop) ; 2
(pop) ; 1
